const assert = require('assert');
const { JSDOM } = require('jsdom');
const jsonUploader = require('../json-uploader');

describe('Array', function () {
  describe('checkJson', () => {
    let textarea;
    let output;

    beforeEach(() => {
      const dom = new JSDOM(`
        <html>
          <body>
            <textarea id="json-input"></textarea>
            <div class="output" id="output"> Invalid JSON </div>
          </body>
        </html>
      `);

      global.window = dom.window;
      global.document = dom.window.document;

      textarea = document.getElementById('json-input');
      output = document.getElementById('output');
    });

    afterEach(() => {
      delete global.window;
      delete global.document;
    });

    it('should set the output text to "Invalid JSON"', () => {
      // Arrange
      const invalidJson = '{"name": "John", "age": 30';

      // Act
      textarea.value = invalidJson;
      jsonUploader.checkJson();

      // Assert
      assert.equal(output.innerHTML, 'Invalid JSON');
    });

    it('should set the output text to "Valid JSON"', () => {
      // Arrange
      const invalidJson = '{"name": "John", "age": 30}';

      // Act
      textarea.value = invalidJson;
      jsonUploader.checkJson();

      // Assert
      assert.equal(output.innerHTML, 'Valid JSON');
    });

  });
});
