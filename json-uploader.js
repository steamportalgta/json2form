window.addEventListener('load', function () {
  const textarea = document.getElementById('json-input');
  textarea.addEventListener('input', checkJson);
});

function generateForm() {
  // Retrieve the JSON data from an input field
  const jsonData = document.getElementById('json-input').value;

  // Parse the JSON data into an object
  const data = JSON.parse(jsonData);

  // Generate the form fields based on the JSON data
  const formHtml = generateFormFields(data);

  // Insert the form fields into the form element
  const form = document.getElementById('json-form');

  form.innerHTML = formHtml;
  form.style.padding = '15px';
  form.scrollIntoView({ behavior: 'smooth' });
}

function addNewTemplate() {
  const textarea = document.getElementById('json-input');
  const currentDate = new Date();
  const formattedDate = `${
    currentDate.getMonth() + 1
  }/${currentDate.getDate()}/${currentDate.getFullYear()}`;

  try {
    const json = `{
        "productCode":"new product",
        "version":"1.0.0",
        "properties":{
          "disableReorderModules":true,
          "hideResults":true,
          "dontGenerateCertificateByDefault":true
      },
      "modules":[
          {
              "id":"---",
              "name":"My First Module",
              "title":"My First Module",
              "displayName":"My First Module",
              "isScorable":false,
              "properties":{
                  
              },
              "pages":[
                  {
                      "id":"---",
                      "title":"My First Page",
                      "name":"My First Page",
                      "displayName":"My First Page",
                      "elements":[
                          {
                              "id":"---",
                              "type":"---"
                          }
                      ]
                  }
              ]
          }
      ],
      "versionInformation":[
          {
              "version":"1.0.0",
              "changeDetails":"Initial Version",
              "createdDate": "${formattedDate}",
              "availableDate": "${formattedDate}"
          }
      ]
    }`;

    textarea.value = json;
    formatJson();
    checkJson();
    generateForm();
  } catch (e) {
    console.log('Unable to generate template');
  }
}

function generateFormFields(data) {
  let html = '';

  for (let key in data) {
    let value = data[key];

    if (typeof value === 'boolean') {
      html += '<label>' + key + ': </label>';
      html +=
        '<input type="checkbox" name="' +
        key +
        '" value="true"' +
        (value ? 'checked' : '') +
        '>';
      html += '<br>';
      // TODO: Improve Array
    } else if (false) {
      html += '<fieldset>';
      html += '<legend>' + key + '</legend>';

      for (let i = 0; i < value.length; i++) {
        html += '<div>';
        html += '<label>' + key + ' ' + (i + 1) + ': </label>';

        if (typeof value[i] === 'object') {
          html += generateFormFields(value[i]);
        } else if (typeof value[i] === 'boolean') {
          html +=
            '<input type="checkbox" name="' +
            key +
            '-' +
            (i + 1) +
            '-checkbox"' +
            (value[i] ? 'checked' : '') +
            '>';
        } else {
          html +=
            '<input type="text" name="' +
            key +
            '-' +
            (i + 1) +
            '-text" value="' +
            value[i] +
            '">';
        }

        html +=
          '<button type="button" onclick="removeFromArray(\'' +
          key +
          "', " +
          i +
          ')">Remove</button>';
        html += '</div>';
      }

      html +=
        '<input type="text" name="' +
        key +
        '-input" placeholder="Add new ' +
        key +
        '">';

      html +=
        '<button type="button" onclick="addToArray(\'' +
        key +
        '\')">Add</button>';

      html += '</fieldset>';
    } else if (typeof value === 'object') {
      html += '<fieldset>';
      html += '<legend>' + key + '</legend>';
      html += generateFormFields(value);

      html += '<div style="display: flex; align-items: center; gap: 10px;">';
      html +=
        '<input type="text" id="' +
        key +
        '-new-property" placeholder="new property">';

      // Add the combobox for selecting the type
      html += '<select id="' + key + '-new-property-type">';
      html += '<option value="object">Object/Array</option>';
      html += '<option value="boolean">Boolean</option>';
      html += '<option value="number">Number</option>';
      html += '<option value="string">String</option>';
      html += '</select>';

      html +=
        '<button class="add-button" onclick="addNewProperty(\'' +
        key +
        '\')">+</button>';
      html += '</div>';

      html += '</fieldset>';
    } else {
      html += '<label>' + key + ': </label>';
      html += '<input type="text" name="' + key + '" value="' + value + '">';
      html += '<br>';
    }
  }

  return html;
}

// function addNewProperty(button, key) {
//   const inputField = button.previousSibling;
//   const value = inputField.value;

//   // Check if the input value is not empty
//   if (value.trim() !== '') {
//     const container = button.parentNode;
//     const inputName = key + '.' + value;
//     const inputHtml = '<label>' + value + ': </label>' +
//                       '<input type="text" name="' + inputName + '" value="">' +
//                       '<br>';
//     container.insertAdjacentHTML('beforebegin', inputHtml);

//     // Clear the input field
//     inputField.value = '';
//   }
// }

// function addNewProperty(button, key) {
//   const inputField = button.previousSibling;
//   const value = inputField.value;

//   // Get the selected type from the combobox
//   const typeSelect = button.previousSibling.previousSibling;
//   const selectedType = typeSelect.value;

//   // Check if the input value is not empty
//   if (value.trim() !== '') {
//     const container = button.parentNode;
//     const inputName = key + '.' + value;

//     // Generate the HTML based on the selected type
//     let inputHtml = '';
//     if (selectedType === 'object') {
//       inputHtml = generateFormFields({});
//     } else if (selectedType === 'boolean') {
//       inputHtml = '<input type="checkbox" name="' + inputName + '" value="">';
//     } else if (selectedType === 'number') {
//       inputHtml = '<input type="number" name="' + inputName + '" value="">';
//     } else if (selectedType === 'string') {
//       inputHtml = '<input type="text" name="' + inputName + '" value="">';
//     }

//     // Insert the new property input field HTML
//     container.insertAdjacentHTML('beforebegin', inputHtml);

//     // Clear the input field
//     inputField.value = '';
//   }
// }

function addNewProperty(key) {
  const inputField = document.getElementById(key + '-new-property');
  const value = inputField.value;

  // Get the selected type from the combobox
  const typeSelect = document.getElementById(key + '-new-property-type');
  const selectedType = typeSelect.value;

  // Check if the input value is not empty
  if (value.trim() !== '') {
    const container = inputField.parentNode;
    const inputName = key + '.' + value;

    console.log('selectedType', selectedType);

    // Generate the HTML based on the selected type
    let inputHtml = '';
    if (selectedType === 'object') {
      inputHtml += '<fieldset>';
      inputHtml += '<legend>' + value + '</legend>';
      inputHtml += generateFormFields({});
      inputHtml +=
        '<div style="display: flex; align-items: center; gap: 10px;">';
      inputHtml +=
        '<input type="text" id="' +
        key +
        '-new-property" placeholder="new property">';
      inputHtml += '<select id="' + key + '-new-property-type">';
      inputHtml += '<option value="object">Object</option>';
      inputHtml += '<option value="array">Array</option>';
      inputHtml += '<option value="boolean">Boolean</option>';
      inputHtml += '<option value="number">Number</option>';
      inputHtml += '<option value="string">String</option>';
      inputHtml += '</select>';
      inputHtml +=
        '<button class="add-button" onclick="addNewProperty(\'' +
        key +
        '\')">+</button>';
      inputHtml += '</div>';
      inputHtml += '</fieldset>';
    } else if (selectedType === 'boolean') {
      inputHtml =
        '<label>' +
        value +
        ': </label><input type="checkbox" name="' +
        inputName +
        '" value=""><br>';
    } else if (selectedType === 'number') {
      inputHtml =
        '<label>' +
        value +
        ': </label><input type="number" name="' +
        inputName +
        '" value=""><br>';
    } else if (selectedType === 'string') {
      inputHtml =
        '<label>' +
        value +
        ': </label><input type="text" name="' +
        inputName +
        '" value=""><br>';
    }

    console.log(inputHtml);

    // Insert the new property input field HTML
    container.insertAdjacentHTML('beforebegin', inputHtml);

    // Clear the input field
    inputField.value = '';
  }
}

function submitForm() {
  // Submit this to database... Not Implemented yet :p
  try {
    // Get the form data
    const form = document.getElementById('json-form');
    const formData = new FormData(form);

    // Convert the form data to a JSON object
    const jsonObject = {};
    formData.forEach(function (value, key) {
      jsonObject[key] = value;
    });

    // Send the JSON data to the server
    let xhr = new XMLHttpRequest();

    xhr.open('POST', 'https://example.com/submit-form');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onload = function () {
      if (xhr.status === 200) {
        alert('Form submitted successfully');
      } else {
        alert('Error submitting form');
      }
    };
    xhr.send(JSON.stringify(jsonObject));
  } catch (e) {
    alert('Error, not database implemented yet');
  }
}

function checkJson() {
  const textarea = document.getElementById('json-input');
  const output = document.getElementById('output');

  try {
    const json = JSON.parse(textarea.value);
    output.innerHTML = 'Valid JSON';
    output.style.backgroundColor = '#4d89a0';
  } catch (e) {
    output.innerHTML = 'Invalid JSON';
    output.style.backgroundColor = '#833241';
  }
}

function formatJson() {
  const textarea = document.getElementById('json-input');

  try {
    const json = JSON.parse(textarea.value);
    const formattedJson = JSON.stringify(json, null, 4);
    textarea.value = formattedJson;
  } catch (e) {
    console.log('unable to Format');
  }
}

// Prevent default form submission behavior
document
  ?.getElementById('json-form')
  ?.addEventListener('submit', function (event) {
    event.preventDefault();
  });

module.exports = {
  checkJson,
  formatJson,
  submitForm,
  generateFormFields,
  generateForm,
};

// TODO: implement this, I didnt have that much time :(
// function generateFormFields(data) {
//   let html = '';

//   for (let key in data) {
//     let value = data[key];

//     if (typeof value === 'boolean') {
//       html += '<label>' + key + ': </label>';
//       html +=
//         '<input type="checkbox" name="' +
//         key +
//         '" value="true"' +
//         (value ? 'checked' : '') +
//         '>';
//       html += '<br>';
//     } else if (typeof value === 'object') {
//       html += '<fieldset>';
//       html += '<legend>' + key + '</legend>';
//       html += generateFormFields(value);
//       html += '</fieldset>';
//     } else {
//       html += '<label>' + key + ': </label>';
//       html +=
//         '<input type="text" name="' + key + '" value="' + value + '">';
//       html += '<br>';
//     }
//   }

//   return html;
// }

// function addToArray(key) {
//   let input = document.querySelector('input[name="' + key + '-input"]');
//   let value = input.value;
//   let arrayInputs = document.querySelectorAll('input[name^="' + key + '"]');
//   let nextIndex = arrayInputs.length;

//   let html = '<br>';
//   html += '<label>' + key + ' ' + nextIndex + ': </label>';

//   if (typeof value === 'object') {
//     html += '<div>';
//     html += generateFormFields(value);
//     html += '</div>';
//   } else if (typeof value === 'boolean') {
//     html +=
//       '<input type="checkbox" name="' + key + '-' + nextIndex + '-checkbox">';
//   } else {
//     html +=
//       '<input type="text" name="' +
//       key +
//       '-' +
//       nextIndex +
//       '-text" value="' +
//       value +
//       '">';
//   }

//   let container = input.previousSibling;
//   container.insertAdjacentHTML('beforeend', html);
// }

// function removeFromArray(key, index) {
//   index = index + 1;

//   let element = document.querySelector(
//     'input[name="' + key + '-' + index + '-text"]'
//   );
//   if (element) {
//     element.parentNode.remove();
//   }
//   element = document.querySelector(
//     'input[name="' + key + '-' + index + '-checkbox"]'
//   );
//   if (element) {
//     element.parentNode.remove();
//   }
// }
